﻿using AudioSwitcher.AudioApi.CoreAudio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _500cc.ArteIterativa.Contoller
{
    public class SoundController
    {
        CoreAudioDevice defaultPlaybackDevice;

        public SoundController()
        {
            defaultPlaybackDevice = new CoreAudioController().DefaultPlaybackDevice;
        }

        public void AdjustVolume(int skeletonCount)
        {
            if (skeletonCount == 1)
            {
                defaultPlaybackDevice.Volume = 40;
            }
            else if (skeletonCount == 2)
            {
                defaultPlaybackDevice.Volume = 80;
            }
        }
    }
}
